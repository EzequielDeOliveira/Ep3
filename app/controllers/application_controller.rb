class ApplicationController < ActionController::Base

include Pundit

before_action :configure_permitted_parameters, if: :devise_controller?

  protect_from_forgery with: :exception

rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized



private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:last_name])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:idade])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:telefone])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:endereco])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:cpf])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:tipo_sanguineo])
  end

  def user_not_authorized
    flash[:notice] = "Você não tem permissão para fazer esta ação"
    redirect_to(request.referrer || root_path)
  end
end
