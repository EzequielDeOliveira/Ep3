Rails.application.routes.draw do

  get 'control_users/index'

  devise_for :users
  root 'welcome#index'
  resources :sangues
  resources :doadors

  resources :faq

  get 'welcome' => 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
