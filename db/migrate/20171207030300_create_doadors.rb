class CreateDoadors < ActiveRecord::Migration[5.1]
  def change
    create_table :doadors do |t|
      t.string :nome
      t.integer :idade
      t.string :telefone
      t.string :email
      t.string :cpf
      t.string :endereco
      t.string :tipo_sanguineo

      t.timestamps
    end
  end
end
